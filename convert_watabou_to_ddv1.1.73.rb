# Ruby Script to parse Watabou One Page Dungeon JSON format into a Dungeondraft map file
# Version 1.1, Updated: 1-Apr-22, Author: u/uchideshi34

require 'json'
require 'optparse'

# CONFIGURATION VARIABLES
$scale = 2
$kragers = true
$config_hash = {
	"header": {
		"config_name": "Default Config",
		"version": "v1.0",
    "asset_style": "Dungeondraft"
  },
  "general": {
    "scale": 2,
    "baseline_map_file": "baseline.dungeondraft_map",
    "terrain_type": "res://textures/terrain/terrain_swamp.png"
  },
  "walls": {
    "wall_texture": "res://textures/walls/cobble.png",
    "wall_colour": "505050",
		"joint": 0
  },
  "floors": {
    "floor_as_pattern": false,
    "floor_layer": -100,
    "room_colour": "ff8c8c8c",
    "floor_texture": "res://textures/tilesets/simple/tileset_concrete.png",
    "tile_texture": "res://textures/tilesets/smart_double/tileset_stone.png"
  },
  "shadows": {
    "use_kragers": false,
    "shadow_width": 1.0,
    "shadow_path_texture": "res://packs/XpMDQfBe/textures/paths/Shadow/ShadowPath/ShadowPathMed75.png",
    "shadow_corner_h_texture": "res://packs/XpMDQfBe/textures/objects/ColorableGradient/Circle/Medium/ColorableGradientHalfCircleMedium75.png",
    "shadow_corner_q_texture": "res://packs/XpMDQfBe/textures/objects/ColorableGradient/Circle/Medium/ColorableGradientQuarterCircleMedium75.png",
		"shadow_corner_f_texture": "res://packs/XpMDQfBe/textures/objects/ColorableGradient/Circle/Medium/ColorableGradientFullCircleMedium50.png"
  },
  "doors": {
		"door_types": [
      {
        "door_id": 0,
        "door_description": "A mirrored version of door id 1 for double doors",
        "door_texture": "res://textures/portals/door_05.png"
      },
      {
        "door_id": 1,
        "door_description": "A regular version of the door of which door id is the mirrored version",
        "door_texture": "res://textures/portals/door_06.png"
      },
      {
        "door_id": 4,
        "door_description": "Portcullis door assumed to be symmetrical",
        "door_texture": "res://textures/portals/door_12.png"
      },
      {
        "door_id": 5,
        "door_description": "A double door in a single square width",
        "door_texture": "res://textures/portals/door_10.png"
      }
    ],
    "draw_secret_doors": true,
    "skip_doorway": true,
    "door_as_objects": false,
    "door_layer": 400,
    "staircase_1x1_texture": "res://textures/objects/structures/stairs/stairs_square_06.png",
    "staircase_1x2_texture": "res://textures/objects/structures/stairs/stairs_10.png",
    "staircase_2x1_texture": "res://textures/objects/structures/stairs/stairs_10.png",
    "staircase_2x2_texture": "res://textures/objects/structures/stairs/stairs_14.png",
    "wide_staircase_type": "1x2"
  },
  "columns": {
    "column_texture": "res://textures/objects/structures/pillars/pillar_stone_32.png",
    "shadow_size": 0.45,
    "shadow_num": 2
  },
  "lights": {
    "mood_lighting": true,
    "mood_colours": ["6480beff","644dd569"],
    "light_density": 0.25,
    "light_range": 3.0,
    "light_wall_distance": 2.0,
    "light_intensity": 0.6,
    "light_texture": "res://textures/lights/soft.png",
    "ambient_mood_light": "ff8c8c8c"
  }
}


# FUNCTION DEFINITIONS
# Add custom asset packs
def add_custom_asset_packs ()
  new_hash = Hash.new
  if $kragers == true
    new_hash = {
      "name": "Krager's Shadow & Light Pack",
      "id": "XpMDQfBe",
      "version": "1.0",
      "author": "Krager",
      "custom_color_overrides": {
        "enabled": false,
        "min_redness": 0,
        "min_saturation": 0,
        "red_tolerance": 0
      }
    }
    $base_hash['header']['asset_manifest'] << new_hash
  end

end

# Scale Map
def scale_map(data_hash)

  data_hash['rects'].each do | floor |
    floor['x'] = floor['x'] * $scale
    floor['y'] = floor['y'] * $scale
    floor['h'] = floor['h'] * $scale
    floor['w'] = floor['w'] * $scale
  end
  data_hash['doors'].each do | door |
    door['x'] = door['x'] * $scale
    door['y'] = door['y'] * $scale
  end
  data_hash['columns'].each do | column |
    column['x'] = column['x'] * $scale
    column['y'] = column['y'] * $scale
  end

end

# Draw Shadow Path
def draw_shadow_path(vector_array, end_point_type, end_turn_type, width)
  shadow_path = {
    "rotation": 0,
    "scale": "Vector2( 1, 1 )",
    "smoothness": 1,
    "layer": 400,
    "fade_in": false,
    "fade_out": false,
    "grow": false,
    "shrink": false,
    "loop": false,
  }
  shadow_path[:texture] = $config_hash[:shadows][:shadow_path_texture]
  shadow_path[:position] = "Vector2( #{vector_array[0]*256}, #{vector_array[1]*256} )"
  shadow_path[:width] = width
  shadow_path[:edit_points] = "PoolVector2Array( 0, 0"
  for i in 1..vector_array.length/2-1 do
    shadow_path[:edit_points] += ", #{(vector_array[i*2]-vector_array[0])*256}, #{(vector_array[i*2+1]-vector_array[1])*256}"
  end
  shadow_path[:edit_points] += " )"
  shadow_path[:node_id] = $next_node_id.to_s(16)
  if end_point_type == 0
    shadow_path[:fade_in] = true
    shadow_path[:fade_out] = true
  end
  $base_hash['world']['levels']['0']['paths'] << shadow_path
  $next_node_id += 1
end

# Draw Corner for Shadow Path
def draw_shadow_corner(vector_array, direction, type)

  if type == "H"
    x = vector_array[0] + delta_in_direction(0.25*$config_hash[:shadows][:shadow_width],0,direction,"x")
    y = vector_array[1] + delta_in_direction(0.25*$config_hash[:shadows][:shadow_width],0,direction,"y")
    texture = $config_hash[:shadows][:shadow_corner_h_texture]
  else
    x = vector_array[0] + delta_in_direction(0.25*$config_hash[:shadows][:shadow_width],0.25*$config_hash[:shadows][:shadow_width],direction,"x")
    y = vector_array[1] + delta_in_direction(0.25*$config_hash[:shadows][:shadow_width],0.25*$config_hash[:shadows][:shadow_width],direction,"y")
    texture = $config_hash[:shadows][:shadow_corner_q_texture]
  end

  # Note that rotation is reversed in DD as y direction is down not up
  rotation = ((- direction + 3).modulo(4) - 1) * Math::PI/2
  draw_object(x,y,rotation,0.5*$config_hash[:shadows][:shadow_width],texture,400,false)
end

# Create a PoolVector string from an array
def pvstr(array, reverse)

  # Leave the order as is
  if reverse == false
    str = "PoolVector2Array("
    (array.length() / 2).times { |i| str = "#{str} #{array[2*i]*256}, #{array[2*i+1]*256},"}
    str = "#{str.chop} )"
    return str
  # Reverse the order of the x, y pairs
  else
    str = " )"
    (array.length() / 2).times { |i| str = ", #{array[2*i]*256}, #{array[2*i+1]*256} #{str}"}
    str[0] = ''
    str[0] = ''
    str = "PoolVector2Array( #{str}"
    return str
  end
end

# Create a PoolInt string from an array
def pistr(array)
  str = "PoolIntArray("
  array.each do |value|
    str = "#{str}#{value},"
  end
  str = "#{str.chop} )"
  return str
end

# Add a light object to the hash
def place_light(x, y, range, intensity, colour, texture)

  light_hash = {
    "rotation": 0,
    "shadows": true
  }

  light_hash[:range] = range
  light_hash[:color] = colour
  light_hash[:intensity] = intensity
  light_hash[:texture] = texture
  light_hash[:position] = "Vector2( #{x*256}, #{y*256} )"
  light_hash[:node_id] = $next_node_id
  $next_node_id += 1
  $base_hash['world']['levels']['0']['lights'] << light_hash

end

# Get scatter point in room with wall_offset defining the minimum distance from the nearest wall
def get_scatter_point(height, width, wall_offset, circular)

  # If the wall_offset is too big then return the centre point
  if height < wall_offset*2 || width < wall_offset*2
    return [width/2,height/2]
  end
  # If circular room then pick a radial option
  if circular
    radius = (height/2-wall_offset) * rand()
    theta = rand()*Math::PI*2
    x = radius * Math.sin(theta) + height/2
    y = radius * Math.cos(theta) + height/2
  # Otherwise choose a random point
  else

    x = rand(0.001..(width-wall_offset*2).to_f()-0.001) + wall_offset

    y = rand(0.001..(height-wall_offset*2).to_f()-0.001) + wall_offset


  end
  return [x,y]
end


# Function to scatter mood lighting around the map
def scatter_mood_lighting()

  mood_colours = Array.new
  scatter_point = Array.new
  mood_colours = $config_hash[:lights][:mood_colours]
  light_density = $config_hash[:lights][:light_density]
  light_texture = $config_hash[:lights][:light_texture]
  light_wall_distance = $config_hash[:lights][:light_wall_distance]
  light_range = $config_hash[:lights][:light_range]
  light_intensity = $config_hash[:lights][:light_intensity]

  $base_hash['world']['levels']['0']['environment']['ambient_light'] = $config_hash[:lights][:ambient_mood_light]

  # Look through all the floor objects and scatter some low intensity lights about
  $polygons.each do | polygon |
    height = polygon[3][1] - polygon[0][1]
    width = polygon[1][0] - polygon[0][0]

    if height > (1 * $scale) && width > (1 * $scale)
      # If the room is circular then place lights radially
      if $map[polygon[0][0]][polygon[0][1]].include?("T")
        for i in 1..(light_density * ((height-light_wall_distance) ** 2) * 0.75).floor()
          scatter_point = get_scatter_point(height, width, light_wall_distance, true)
          place_light(polygon[0][0]+scatter_point[0],polygon[0][1]+scatter_point[1], light_range, light_intensity, mood_colours[rand(0..(mood_colours.length-1))], light_texture)
        end
      else
        # if the distance from the wall means no lights can be placed then skip
        if light_wall_distance * 2 >= height || light_wall_distance * 2 >= width
          next
        end
        for i in 1..(light_density * (height-light_wall_distance) * (width-light_wall_distance)).floor()

            scatter_point = get_scatter_point(height, width, light_wall_distance, false)

            place_light(polygon[0][0]+scatter_point[0].to_f(), polygon[0][1]+scatter_point[1].to_f(), light_range, light_intensity, mood_colours[rand(0..(mood_colours.length-1))], light_texture)

        end
      end
    elsif height > 3 || width > 3
      # If this is a corridor then place a few lights

      for i in 1..([height,width].max()/3).floor() do
        scatter_point = get_scatter_point(height, width, 0.4*$scale, false)

        place_light(polygon[0][0]+scatter_point[0].to_f(), polygon[0][1]+scatter_point[1].to_f(), light_range, light_intensity, mood_colours[rand(0..(mood_colours.length-1))], light_texture)
      end
    # If a single 1x1 square then place a single small light
    else
      scatter_point = get_scatter_point(height, width, 0.25*$scale, false)
      place_light(polygon[0][0]+scatter_point[0].to_f(), polygon[0][1]+scatter_point[1].to_f(), 2, light_intensity, mood_colours[rand(0..(mood_colours.length-1))], light_texture)
    end
  end

end

# Return an array of deltas to x and y in a direction
def delta_in_direction(nf,nr,d,type)
  array = [0,0]
  if d.odd?
    array[0] += (2 - d) * nf
  else
    array[1] += (1 - d) * nf
  end
  if ((d+1).modulo(4)).odd?
    array[0] += (2 - (d+1).modulo(4)) * nr
  else
    array[1] += (1 - (d+1).modulo(4)) * nr
  end
  if type == "x"
    return array[0]
  else
    return array[1]
  end
end

# Draw a circle of point based on an edge point and a centre point
def draw_circle(x, y, centre_point_x, centre_point_y)

  # Set up variables
  array = Array.new
  radius = (centre_point_x - x).to_f().abs

  # Calculate an incremental angle that will lay around 4 points per square
  theta = Math.asin(1/radius)/4

  # Initialise the array with the start points
  angle = 0

  # Iterate creating points and adding them to the array
  for i in 0..(Math::PI * 2 / theta).floor - 1 do
    array << (radius * Math.cos(angle) + centre_point_x).round(6)
    array << (radius * Math.sin(angle) + centre_point_y).round(6)
    angle += theta
  end

  # return the array
  return array
end

# Draw a floor pattern
def draw_floor_pattern(polygon,circular)

  pattern = {
    "position": "Vector2( 0, 0 )",
    "shape_rotation": 0,
    "scale": "Vector2( 1, 1 )",
    "outline": false,
    "rotation": 0,
  }

  pattern[:color] = $config_hash[:floors][:room_colour]
  pattern[:texture] = $config_hash[:floors][:floor_texture]
  pattern[:layer] = $config_hash[:floors][:floor_layer]

  if circular
    height = polygon[3][1] - polygon[0][1]
    width = polygon[1][0] - polygon[0][0]
    circle = Array.new
    circle = draw_circle(polygon[0][0], polygon[0][1], polygon[0][0]+width/2, polygon[0][1]+height/2)
    pattern[:points] = pvstr(circle, false)
  else
    pattern[:points] = pvstr(polygon.flatten, false)
  end

  pattern[:node_id] = $next_node_id.to_s(16)
  $next_node_id += 1
  $base_hash['world']['levels']['0']['patterns'] << pattern
end

# Display polygon
def print_polygon(poly_array)
  for i in 0..poly_array.length-1 do
    puts "#{i}, #{poly_array[i][0]},#{poly_array[i][1]}"
  end
end

def print2_polygon(poly_array)
  for i in 0..poly_array.length-1 do
    print "[#{poly_array[i][0]},#{poly_array[i][1]}],"
  end
  puts ""
  $stdout.flush
end

# Look for x,y coordinates that sit on a vertex between vertices on the other polygon
def is_between(x1,y1,a1,a2,b1,b2)
  if a1 == a2 && a2 == x1
    #puts "x1 on the line with a1 and a2, x: #{x1}, y: #{y1}, a1: #{a1}, b1: #{b1}, a2: #{a2}, b2: #{b2}"
    if (y1 < b1 && y1 > b2) || (y1 > b1 && y1 < b2)
      #puts "y1 between b1 and b2, x: #{x1}, y: #{y1}, a1: #{a1}, b1: #{b1}, a2: #{a2}, b2: #{b2}"
      return "x"
    elsif y1 == b1
      return "a1b1"
    elsif y1 == b2
      return "a2b2"
    end
  elsif b1 == b2 && b2 == y1
    #puts "y1 on the line with b1 and b2, x: #{x1}, y: #{y1}, a1: #{a1}, b1: #{b1}, a2: #{a2}, b2: #{b2}"
    if ( x1 < a1 && x1 > a2 ) || ( x1 > a1 && x1 < a2 )
      #puts "x1 between a1 and a2, x: #{x1}, y: #{y1}, a1: #{a1}, b1: #{b1}, a2: #{a2}, b2: #{b2}"
      return "y"
    elsif x1 == a1
      return "a1b1"
    elsif x1 == a2
      return "a2b2"
    end
  end
  return 0
end

# Merge two polygons together assuming they are written anticlockwise
# Requires both polygons are in this format: [[0,1],[3,1],[3,3],[0,3]] with vertices following an anti-clockwise direction
def merge_polygons(polygon1,polygon2)
  found = [-1,-1]
  # For each vertex in polygon1
  for i in 0..polygon1.length - 1 do
    x1 = polygon1[i][0]
    y1 = polygon1[i][1]
    x2 = polygon1[(i+1).modulo(polygon1.length)][0]
    y2 = polygon1[(i+1).modulo(polygon1.length)][1]

    # For each vertex on Polygon2 Look to see if the x,y vertex sits between two others
    for j in 0..polygon2.length - 1 do
      a1 = polygon2[j][0]
      b1 = polygon2[j][1]
      a2 = polygon2[(j+1).modulo(polygon2.length)][0]
      b2 = polygon2[(j+1).modulo(polygon2.length)][1]
      #puts "x1: #{x1}, y1: #{y1}, i: #{i}, j: #{j}, next_j: #{(j+1).modulo(polygon1.length - 1)},a1: #{a1},  b1: #{b1}, a2: #{a2}, b2: #{b2}"

      # If x1 is on a line with a1 and a2
      if is_between(x1,y1,a1,a2,b1,b2) == "x"
        # If the following point x2 is between a1 and a2 then use x1 or jump back one point to x0
        if ["x","a1b1"].include?(is_between(x2,y2,a1,a2,b1,b2))
          found = [(i).modulo(polygon1.length),(j+1).modulo(polygon2.length)]
        else
          found = [(i-1).modulo(polygon1.length),(j+1).modulo(polygon2.length)]
        end
        break
      elsif is_between(x1,y1,a1,a2,b1,b2) == "y"
        # If the following point x2 is between a1 and a2 then use x1 or jump back one point to x0
        if ["y","a1b1"].include?(is_between(x2,y2,a1,a2,b1,b2))
          found = [(i).modulo(polygon1.length),(j+1).modulo(polygon2.length)]
        else
          found = [(i-1).modulo(polygon1.length),(j+1).modulo(polygon2.length)]
        end
        break
      elsif is_between(x1,y1,a1,a2,b1,b2) == "a1b1"
        # Look ahead at the next point [x2,y2] is there
        if ["x","y"].include?(is_between(x2,y2,a1,a2,b1,b2))
          found = [(i).modulo(polygon1.length),(j+1).modulo(polygon2.length)]
          break
        # Check that the previous point [x0, y0] is a valid join
        elsif ["x","y","a1b1","a2b2"].include?(is_between(polygon1[(i-1).modulo(polygon1.length)][0],polygon1[(i-1).modulo(polygon1.length)][1],a1,a2,b1,b2))
          found = [(i-1).modulo(polygon1.length),(j+1).modulo(polygon2.length)]
          break
        else
          # Matches only one vertex so no join
        end
      elsif is_between(x1,y1,a1,a2,b1,b2) == "a2b2"
        # Do nothing and wait until
      end

    end
    break if found[0] > -1
  end

  # If there is a match between vertices
  if found[0] > -1
    # Add all the points for polygon2 into polygon1 list at the right point
    for j in 0..polygon2.length - 1 do
      a1 = polygon2[(j+found[1]).modulo(polygon2.length)][0]
      b1 = polygon2[(j+found[1]).modulo(polygon2.length)][1]
      polygon1.insert(found[0]+1+j, [a1,b1])
    end

    # Remove the duplicate points that follow each other
    for k in 0..polygon1.length
      delete_ref = 0
      # For each point in the polygon1
      for i in 0..polygon1.length - 1 do
        # If one point matches the next point, delete it
        if polygon1[i] == polygon1[(i+1).modulo(polygon1.length)]
          if i < polygon1.length - 1
            polygon1.delete_at(i)
            polygon1.delete_at(i)
          else
            polygon1.delete_at(polygon1.length - 1)
            polygon1.delete_at(0)
          end
          delete_ref = 1
          break
        end
      end
      break if delete_ref == 0
    end
    return true
  else
    return false
  end
end

# Merge polygons but ensure the output is always referenced as polygon1
# Requires both polygons are in this format: [[0,1],[3,1],[3,3],[0,3]] with vertices following an clockwise direction
def full_merge_polygons(polygon1,polygon2)
  if merge_polygons(polygon1,polygon2)
    polygon1 << 1
    return polygon1
  elsif merge_polygons(polygon2,polygon1)
    polygon2 << 2
    return polygon2
  else
    polygon1 << 0
    return polygon1
  end
end

# Draw patterns for each corridor merging as needed
def merge_corridors(corridors_array)

  for index in 0..(corridors_array.length-1)
    final_polygon = corridors_array[0]
    #puts "Iteration #{index}"
    #puts "final_polygon.length #{final_polygon.length}"
    #puts "corridors_array.length #{corridors_array.length}"
    for i in 1..(corridors_array.length-1) do
      check_any_found = 0
      # Check each other corridor array (except the first on which is primed as final_polygon)
      for k in 1..(corridors_array.length-1) do
        final_polygon = full_merge_polygons(final_polygon,corridors_array[k])
        if final_polygon[-1] == 1
          #puts "Matched with type 1 so remove polygon#{k}"
          #print2_polygon(corridors_array[k])
          corridors_array.delete_at(k)

          check_any_found = 1
          final_polygon.pop

          break
        elsif final_polygon[-1] == 2
          #puts "Matched with type 2 so remove polygon#{k}"
          #print2_polygon(corridors_array[k])

          final_polygon.pop
          corridors_array.delete_at(k)

          check_any_found = 1
          break
        elsif final_polygon[-1] == 0

          final_polygon.pop
        else
          puts "Error with polygon last element #{final_polygon[-1]}"
          final_polygon.pop
          break
        end

      end

      # If no matches found then break out and start with a new initial corridor
      if check_any_found == 0
        #puts "No match found with any polygon #{corridors_array.length}, iterations taken: #{i}"
        break
      end
    end
    # We've finished with this corridor so write it out and reinitialise
    # Draw out a floor pattern for the corridor
    draw_floor_pattern(final_polygon, false)
    #If this is the last corridor then stop, otherwise reinitialise
    if corridors_array.length == 1
      break
    else
      #Remove this corridor
      corridors_array.delete_at(0)
    end
  end
end

# Draw patterns for each room
def draw_room_floor(rooms_array)

  # For each room create a pattern
  for i in 0..rooms_array.length-1 do
    if $map[rooms_array[i][0][0]][rooms_array[i][0][1]].include?("T")
      draw_floor_pattern(rooms_array[i],true)
    else
      draw_floor_pattern(rooms_array[i],false)
    end
  end
end

# Draw patterns for each room and corridor
def create_pattern_floor ()
  corridors_array = Array.new
  rooms_array = Array.new
  # Create new arrays of corridors and rooms
  for i in 0..($polygons.length-1) do
    # Check if the polygon represents a corridor rather than a room
    height = $polygons[i][3][1] - $polygons[i][0][1]
    width = $polygons[i][1][0] - $polygons[i][0][0]
    if height == $scale || width == $scale
      corridors_array << $polygons[i]
    else
      rooms_array << $polygons[i]
    end
  end

  merge_corridors(corridors_array)
  draw_room_floor(rooms_array)
end

# Return an array of points in x,y form based on a start, end, centre and radius, try to get 3-4 points per square
# Note that this assumes the room is always on the right, which it should be, so we are always going clockwise
def draw_curve(start_point_x, start_point_y, end_point_x, end_point_y, centre_point_x, centre_point_y)

  # Set up variables
  array = Array.new
  radius = ((end_point_x - centre_point_x)**2 + (end_point_y - centre_point_y)**2)**0.5

  # Check that the inputs are right
  if (((start_point_x - centre_point_x)**2 + (start_point_y - centre_point_y)**2)**0.5).round(5) != radius.round(5)
    puts "Error in draw_curve setting up radius"
    exit
  end
  # Calculate an incremental angle that will lay around 4 points per square
  theta = Math.asin(1/radius)/4
  # Calculate the start point angle
  start_angle = Math.atan2((start_point_y - centre_point_y).to_f,(start_point_x - centre_point_x).to_f)
  # Calculate the end point angle
  end_angle = Math.atan2((end_point_y - centre_point_y).to_f,(end_point_x - centre_point_x).to_f)

  full_angle = start_angle - end_angle
  if full_angle < 0
    full_angle += Math::PI * 2
  end

  # Initialise the array with the start points
  angle = start_angle
  array << start_point_x
  array << start_point_y

  # Iterate creating points and adding them to the array
  for i in 0..(full_angle / theta).floor - 1 do
    angle -= theta
    array << (radius * Math.cos(angle) + centre_point_x).round(6)
    array << (radius * Math.sin(angle) + centre_point_y).round(6)
  end

  # Add the end points as the final part of the array
  array << end_point_x
  array << end_point_y

  # return the array
  return array
end

# Create an array for a polygon
def create_polygon(x,y,w,h)
  array = Array.new
  array[0] = [x,y]
  array[1] = [(x+w),y]
  array[2] = [(x+w),(y+h)]
  array[3] = [x,(y+h)]
  return array
end

# Find the size of the floor
def find_floor_size(data_hash)
  map_size = [0,0,0,0]
  floor_count = 0
  data_hash['rects'].each do |floor|
    if floor['x'] < map_size[0]
      map_size[0] = floor['x']
    end
    if floor['y'] < map_size[1]
      map_size[1] = floor['y']
    end
    if floor['x'] + floor['w'] > map_size[2]
      map_size[2] = floor['x'] + floor['w']
    end
    if floor['y'] + floor['h'] > map_size[3]
      map_size[3] = floor['y'] + floor['h']
    end
    floor_count += 1
  end
  map_size[2] = map_size[2] - map_size[0] + 2
  map_size[3] = map_size[3] - map_size[1] + 2
  return map_size
end

# Build a vertex based map system and record into $map and also create a list of ordered floor objects in $polygons
def build_map_file(map_size, data_hash)
  # Read through each rectangle defined in the Watabou JSON file and convert it into a vertex system
  k = 0
  data_hash['rects'].each do |floor|
    #Convert references to new map references
    floor['x'] = floor['x'] - map_size[0] + 1
    floor['y'] = floor['y'] - map_size[1] + 1

    # If the rects object is a room then draw in room values
    $polygons[k] = create_polygon(floor['x'],floor['y'],floor['w'],floor['h'])
    k += 1
    if floor['h'] > 1 * $scale && floor['w'] > 1 * $scale
      if floor.has_key?('rotunda')
        type = "T"
      else
        type = "R"
      end
    else
      type = "C"
    end
    # Do bottom left corner
    $map[floor['x']][floor['y']][1] = type
    # Do bottom right corner
    $map[floor['x']+floor['w']][floor['y']][0] = type
    # Do top left corner
    $map[floor['x']][floor['y']+floor['h']][2] = type
    # Do top right corner
    $map[floor['x']+floor['w']][floor['y']+floor['h']][3] = type

    # Do bottom edge
    for i in floor['x']+1..floor['x']+floor['w']-1 do
      $map[i][floor['y']][0] = type
      $map[i][floor['y']][1] = type
    end
    # Do top edge
    for i in floor['x']+1..floor['x']+floor['w']-1 do
      $map[i][floor['y']+floor['h']][2] = type
      $map[i][floor['y']+floor['h']][3] = type
    end
    # Do left edge
    for i in floor['y']+1..floor['y']+floor['h']-1 do
      $map[floor['x']][i][1] = type
      $map[floor['x']][i][2] = type
    end
    # Do right edge
    for i in floor['y']+1..floor['y']+floor['h']-1 do
      $map[floor['x']+floor['w']][i][0] = type
      $map[floor['x']+floor['w']][i][3] = type
    end

    # Fill in the middle area
    for i in floor['x']+1..floor['x']+floor['w']-1 do
      for j in floor['y']+1..floor['y']+floor['h']-1 do
        $map[i][j] = "#{type}#{type}#{type}#{type}-0"
      end
    end
  end

  # Add references for single width doors so we can do a little instep
  data_hash['doors'].each do |door|

    #Convert references to new map references
    door['x'] = door['x'] - map_size[0] + 1
    door['y'] = door['y'] - map_size[1] + 1
    door['h'] = 1 * $scale
    door['w'] = 1 * $scale


    # This type "D" is used to modify walls for single width doors, if not a single width door then skip
    if [0,2,3,5,6,8].include?(door['type']) || $scale == 1 || $config_hash[:doors][:skip_doorway]
      next
    end

    type = "D"
    # Do bottom left corner
    $map[door['x']][door['y']][1] = type
    # Do bottom right corner
    $map[door['x']+door['w']][door['y']][0] = type
    # Do top left corner
    $map[door['x']][door['y']+door['h']][2] = type
    # Do top right corner
    $map[door['x']+door['w']][door['y']+door['h']][3] = type

    # Do bottom edge
    for i in door['x']+1..door['x']+door['w']-1 do
      $map[i][door['y']][0] = type
      $map[i][door['y']][1] = type
    end
    # Do top edge
    for i in door['x']+1..door['x']+door['w']-1 do
      $map[i][door['y']+door['h']][2] = type
      $map[i][door['y']+door['h']][3] = type
    end
    # Do left edge
    for i in door['y']+1..door['y']+door['h']-1 do
      $map[door['x']][i][1] = type
      $map[door['x']][i][2] = type
    end
    # Do right edge
    for i in door['y']+1..door['y']+door['h']-1 do
      $map[door['x']+door['w']][i][0] = type
      $map[door['x']+door['w']][i][3] = type
    end

    # Fill in the middle area
    for i in door['x']+1..door['x']+door['w']-1 do
      for j in door['y']+1..door['y']+door['h']-1 do
        $map[i][j] = "#{type}#{type}#{type}#{type}-0"
      end
    end
  end

  # Sort the floor objects into order
  $polygons.sort!
end

# Draw a wall for a circular room
def draw_rotunda_wall(wall,walli,wallj,direction)

  # Define output array [end_point_x,end_point_x,direction]
  output = [0,0,0]
  centre_point_x = -1.0
  centre_point_y = -1.0
  start_point_x = walli
  start_point_y = wallj
  shadow_path = Array.new
  shadow_path[0] = start_point_x
  shadow_path[1] = start_point_y


  # Look for the exit from from the rotunda and take one step first
  count = 0
  $map[walli][wallj][5]="1"
  direction = (direction - 1).modulo(4)
  walli += delta_in_direction(1,0,direction,"x")
  wallj += delta_in_direction(1,0,direction,"y")
  count += 1

  while count < 1000 do
    # Look at new map location and decide whether to stop, go straight, turn left or turn right
    # This condition should not occur
    if $map[walli][wallj][5] == "1" && count > 0
      break
    # Check if the corridor turns left
    elsif ["D","C","R","T"].include?($map[walli][wallj][direction.modulo(4)]) && ["D","C","R","T"].include?($map[walli][wallj][(direction + 1).modulo(4)])
      # Turn left if corridor is on the right
      # This is the exit to a rotunda, noting we always enter at a left turn. So this is the end point of the curve
      direction = (direction - 1).modulo(4)
      end_point_x = walli
      end_point_y = wallj
      $map[walli][wallj][5]="1"
      break
    # This is walking around the internal area of the "square room" looking for the corner which will tell us how big the room is
    elsif ["0"].include?($map[walli][wallj][direction.modulo(4)]) && ["0"].include?($map[walli][wallj][(direction + 1).modulo(4)])
      # Turn right and keep going
      if centre_point_x < 0
        # Look at the direction we came in on and find the centre point
        case (direction + 1).modulo(4)
        when 0
          centre_point_x = start_point_x + 0.5 * $scale
          centre_point_y = start_point_y + count + 0.5 * $scale
        when 1
          centre_point_x = start_point_x + count + 0.5 * $scale
          centre_point_y = start_point_y - 0.5 * $scale
        when 2
          centre_point_x = start_point_x - 0.5 * $scale
          centre_point_y = start_point_y - count - 0.5 * $scale
        when 3
          centre_point_x = start_point_x - count - 0.5 * $scale
          centre_point_y = start_point_y + 0.5 * $scale
        end
      end
      direction = (direction + 1).modulo(4)
    # Go straight if the final condition holds. This condition should not occur
    elsif ["0C","C0","0R","R0","0T","T0"].include? ($map[walli][wallj][direction.modulo(4)] + $map[walli][wallj][(direction + 1).modulo(4)])

    else
      puts "Error in draw_rotunda_wall"
    end

    # Set the vertex to having been mapped to a wall
    $map[walli][wallj][5]="1"
    walli += delta_in_direction(1,0,direction,"x")
    wallj += delta_in_direction(1,0,direction,"y")
    count += 1
  end

  temp_array = draw_curve(start_point_x, start_point_y, end_point_x, end_point_y, centre_point_x, centre_point_y)
  temp_array.each do |element|
    wall << element
    shadow_path << element
  end
  if $kragers == true
    draw_shadow_path(shadow_path, 0, 1, 256*$config_hash[:shadows][:shadow_width])
  end

  output[0] = walli
  output[1] = wallj
  output[2] = direction
  return output

end

# Draw a little doorway and return the wall array, assume that this starts at the entrance and start point is already written
def draw_doorway(start_x,start_y,direction)
  output_array = Array.new
  shadow_path = Array.new

  shadow_path = [start_x,start_y]

  walli = start_x
  wallj = start_y

  # Go forward half a square
  walli += delta_in_direction(0.75,0,direction,"x")
  wallj += delta_in_direction(0.75,0,direction,"y")
  output_array << walli
  output_array << wallj
  if $kragers == true

    shadow_path << walli
    shadow_path << wallj
    draw_shadow_path(shadow_path, 1, 1,256*$config_hash[:shadows][:shadow_width])
    shadow_path = [walli,wallj]
  end

  # Turn right and go half a square
  walli += delta_in_direction(0,0.5,direction,"x")
  wallj += delta_in_direction(0,0.5,direction,"y")
  output_array << walli
  output_array << wallj
  if $kragers == true
    shadow_path << walli
    shadow_path << wallj
    draw_shadow_path(shadow_path, 1, 1,256*$config_hash[:shadows][:shadow_width])
    shadow_path = [walli,wallj]
    draw_shadow_corner(shadow_path,(direction+1).modulo(4),"Q")
  end


  # Go forward a square
  walli += delta_in_direction(0.5,0,direction,"x")
  wallj += delta_in_direction(0.5,0,direction,"y")
  output_array << walli
  output_array << wallj
  if $kragers == true
    shadow_path << walli
    shadow_path << wallj
    draw_shadow_path(shadow_path, 1, 1,256*$config_hash[:shadows][:shadow_width])
    shadow_path = [walli,wallj]
    draw_shadow_corner(shadow_path, direction,"Q")
  end

  # Go left half a square
  walli += delta_in_direction(0,-0.5,direction,"x")
  wallj += delta_in_direction(0,-0.5,direction,"y")
  output_array << walli
  output_array << wallj
  if $kragers == true
    shadow_path << walli
    shadow_path << wallj
    draw_shadow_path(shadow_path, 1, 1,256*$config_hash[:shadows][:shadow_width])
    shadow_path = [walli,wallj]
  end

  # Go forward half a square
  walli += delta_in_direction(0.75,0,direction,"x")
  wallj += delta_in_direction(0.75,0,direction,"y")
  output_array << walli
  output_array << wallj
  if $kragers == true
    shadow_path << walli
    shadow_path << wallj
    draw_shadow_path(shadow_path, 1, 1,256*$config_hash[:shadows][:shadow_width])
  end

  return output_array

end

# Draw out a corridor wall
def draw_corridor_wall(walli,wallj)

  # Having found a start point move start initiate a wall drawing
  wall = Array.new
  wall << walli
  wall << wallj
  shadow_path = [walli,wallj]

  # Set direction to ensure that the room or corridor is on the right
  # The vertex is surrounded by 3 blank spaces (noting that the string always ends with "-0")
  if $map[walli][wallj].count("0") == 4
    direction = ($map[walli][wallj].gsub(/[RTD]/,"C").index("C")-1).modulo(4)
  # The vertex is surrounded by 3 corridor/room spaces (noting that the string always ends with "-0")
  elsif $map[walli][wallj].count("0") == 2
    direction = $map[walli][wallj].index("0")
  # The vertex is part of a straight corridor
  else
    # check the location of the blank spaces
    if $map[walli][wallj][$map[walli][wallj].index("0")+1.modulo(4)] == "0"
      direction = ($map[walli][wallj].index("0")+1).modulo(4)
    else
      direction = 0
    end
  end

  # Run a wall until you hit another room or loop round to the other end of the wall and have to stop
  count = 0
  while count < 1000 do
    # Look at new map location and decide whether to stop, go straight, turn left or turn right

    # Check if the corridor turns left
    if ["D","C","R","T"].include?($map[walli][wallj][direction.modulo(4)]) && ["D","C","R","T"].include?($map[walli][wallj][(direction + 1).modulo(4)])
      # Turn left if corridor is on the right
      # Check if this is the entrance to a rotunda, noting we always enter at a left turn
      if ["T"].include?($map[walli][wallj][direction.modulo(4)])
        # Start drawing curved walls as part the rotunda, output is in form [x,y,direction]
        # Look and see if we have looped back to the beginning
        if $map[walli][wallj][5] == "1" && count > 0
          break
        else
          shadow_path << walli
          shadow_path << wallj
          # Terminate the shadow path at the entrance, place a corner piece and start again
          if $kragers == true
            draw_shadow_path(shadow_path, 1, 1,256*$config_hash[:shadows][:shadow_width])
            shadow_path = [walli,wallj]
            draw_shadow_corner(shadow_path, direction,"H")
          end

          rotunda_out = Array.new
          rotunda_out = draw_rotunda_wall(wall,walli,wallj,direction)
          walli = rotunda_out[0]
          wallj = rotunda_out[1]
          shadow_path = [walli,wallj]
          direction = rotunda_out[2]

          if $kragers == true
            draw_shadow_corner(shadow_path, (direction-2).modulo(4),"H")
          end
        end
      else
        # Look and see if we have looped back to the beginning
        if $map[walli][wallj][5] == "1" && count > 0
          if $kragers == true
            shadow_path << walli
            shadow_path << wallj
            draw_shadow_path(shadow_path, 1, 1,256*$config_hash[:shadows][:shadow_width])
            shadow_path = [walli,wallj]
            draw_shadow_corner(shadow_path, direction,"Q")
          end
          break
        else
          wall << walli
          wall << wallj
          if $kragers == true
            shadow_path << walli
            shadow_path << wallj
            draw_shadow_path(shadow_path, 1, 1,256*$config_hash[:shadows][:shadow_width])
            shadow_path = [walli,wallj]
            draw_shadow_corner(shadow_path, direction,"Q")
          end
          direction = (direction - 1).modulo(4)
        end
      end
      # Check if the corridor turns right
    elsif ["0"].include?($map[walli][wallj][direction.modulo(4)]) && ["0"].include?($map[walli][wallj][(direction + 1).modulo(4)])
      # Look and see if we have looped back to the beginning
      if $map[walli][wallj][5] == "1" && count > 0
        # Terminate the shadow path as we have stopped
        if $kragers == true
          shadow_path << walli
          shadow_path << wallj
          draw_shadow_path(shadow_path, 1, 1,256*$config_hash[:shadows][:shadow_width])
          shadow_path = [walli,wallj]
        end
        break
      else
        # Turn left is corridor is on the left
        direction = (direction + 1).modulo(4)
        wall << walli
        wall << wallj
        # Terminate the shadow path as we have reached a corner and start a new one
        if $kragers == true
          shadow_path << walli
          shadow_path << wallj
          draw_shadow_path(shadow_path, 1, 1,256*$config_hash[:shadows][:shadow_width])
          shadow_path = [walli,wallj]
        end
      end
    # Go straight if the final condition holds
    elsif ["0C","C0","0R","R0","0T","T0"].include? ($map[walli][wallj][direction.modulo(4)] + $map[walli][wallj][(direction + 1).modulo(4)])
      # Look and see if we have looped back to the beginning - although we should never have done so as we start on a corner
      if $map[walli][wallj][5] == "1" && count > 0
        if $kragers == true
          shadow_path << walli
          shadow_path << wallj
          draw_shadow_path(shadow_path, 1, 1,256*$config_hash[:shadows][:shadow_width])
        end
        break
      end
    # Check and see if we are in a doorway area and draw and narrow doorway if so
    elsif ["0D","D0"].include? ($map[walli][wallj][direction.modulo(4)] + $map[walli][wallj][(direction + 1).modulo(4)])
      if $map[walli][wallj][5] == "1" && count > 0
        if $kragers == true
          shadow_path << walli
          shadow_path << wallj
          draw_shadow_path(shadow_path, 1, 1,256*$config_hash[:shadows][:shadow_width])
        end
        break
      end
      # We could have jumped ahead so jump back as needed
      if ["0D","D0"].include? ($map[walli][wallj][(direction+2).modulo(4)] + $map[walli][wallj][(direction + 3).modulo(4)])
        walli += delta_in_direction(-1,0,direction,"x")
        wallj += delta_in_direction(-1,0,direction,"y")
      end

      if $kragers == true
        # Check if there is no shadow path to terminate and do so if needed
        if walli == shadow_path[0] && wallj == shadow_path[1]
          # Do nothing as there is no shadow path to terminate
        else
          shadow_path << walli
          shadow_path << wallj
          draw_shadow_path(shadow_path, 1, 1,256*$config_hash[:shadows][:shadow_width])
        end
      end

      $map[walli][wallj][5]="1"

      draw_doorway_out = Array.new
      draw_doorway_out = draw_doorway(walli,wallj,direction)
      wall += draw_doorway_out
      # Record these as the next steps but the remove them from main wall as they will be captured as normal
      walli = wall[-2]
      wallj = wall[-1]
      wall.pop
      wall.pop

      count +=1
      if $kragers == true
        # Reset the shadow_path
        shadow_path = [walli,wallj]
      end

      next
    else
      puts "Error in draw_wall"
    end
    # Set the vertex to having been mapped to a wall
    $map[walli][wallj][5]="1"
    walli += delta_in_direction(1,0,direction,"x")
    wallj += delta_in_direction(1,0,direction,"y")
    count += 1
  end
  return wall
end

#Draw a wall from an array of points

def draw_wall(wall_array, reverse, loop)
  wall_hash = {
    "texture": $config_hash[:walls][:wall_texture],
    "color": $config_hash[:walls][:wall_colour],
    "type": 1,
    "joint": $config_hash[:walls][:joint],
    "normalize_uv": true,
    "shadow": false,
    "portals": [

    ]
  }
  wall_hash[:loop] = loop
  wall_hash[:points] = pvstr(wall_array, reverse)
  wall_hash[:node_id] = $next_node_id.to_s(16)
  $next_node_id += 1
  $base_hash['world']['levels']['0']['walls'] << wall_hash
end

# Draw walls using the start points of the floor objects
def draw_all_walls()
  # Draw walls
  $polygons.each do |polygon|
    # Check whether the vertex already has a wall and if not start drawing
    if $map[polygon[0][0]][polygon[0][1]][5] == "0"
      if $map[polygon[0][0]][polygon[0][1]].include?("T")
        next
      end

      # Draw the wall from the vertex bottom left corner
      wall_array = Array.new
      wall_array = draw_corridor_wall(polygon[0][0],polygon[0][1])
      draw_wall(wall_array,true,true)
    end
  end
end

# Create the floor of smart tiles
def create_smart_tile_floor(map_size)
  # For Smart Tiles, set up the colours and references to the texture
  poolintarray = Array.new(map_size[2]*map_size[3],-1)

  # Look up the right tile_reference
  tile_ref = -1
  $base_hash['world']['levels']['0']['tiles']['lookup'].each do |tile_type,value|
    if value == $config_hash[:floors][:tile_texture]
      tile_ref = tile_type
    end
  end
  if tile_ref == -1
    puts "Can not find smart tile reference in baseline map file: #{$config_hash[:floors][:tile_texture]}"
    exit
  end

  # For each square write in where there is a corridor or room
  for x in 0..map_size[2]-1 do
    for y in 0..map_size[3]-1 do
      if ["R","T"].include?($map[x][y][1])
        $base_hash['world']['levels']['0']['tiles']['colors'][y * (map_size[2])+x] = $config_hash[:floors][:room_colour]
        poolintarray[y * (map_size[2])+x] = tile_ref
      elsif ["D","C"].include?($map[x][y][1])
        $base_hash['world']['levels']['0']['tiles']['colors'][y * (map_size[2])+x] = $config_hash[:floors][:room_colour]
        poolintarray[y * (map_size[2])+x] = tile_ref
			else
				$base_hash['world']['levels']['0']['tiles']['colors'][y * (map_size[2])+x] = "ffffffff"
			end
    end
  end
	$base_hash['world']['levels']['0']['tiles']['cells'] = pistr(poolintarray)

  # The walls can be used as maps for the building tool vector references
  $base_hash['world']['levels']['0']['walls'].each do |wall|
    $base_hash['world']['levels']['0']['shapes']['polygons'] << wall[:points]
  end
end

# Write an object into the overall hash
def draw_object(x,y,rotation,size,texture,layer,shadow)
  object_hash = {"mirror": false}
  object_hash[:position] = "Vector2( #{x*256}, #{y*256} )"
  object_hash[:texture] = texture
  object_hash[:layer] = layer
  object_hash[:scale] = "Vector2( #{size}, #{size} )"
  object_hash[:rotation] = rotation
  object_hash[:shadow] = shadow
  object_hash[:custom_color] = "ff000000"
  object_hash[:node_id] = $next_node_id
  $next_node_id += 1
  $base_hash['world']['levels']['0']['objects'] << object_hash
end

# Draw a normal door
def draw_normal_door(x, y, dir_x, dir_y, type)


  # Make a proper door
  portal_hash = {
    "scale": "Vector2( 1, 1 )",
    "radius": 128,
    "wall_id": "ffffffff",
    "wall_distance": 0,
    "closed": true,
  }
  if dir_x == 0
    portal_hash[:position] = "Vector2( #{x*256}, #{y*256} )"
    portal_hash[:direction] = "Vector2( 1, 0 )"
    portal_hash[:rotation] = 0
  else
    portal_hash[:position] = "Vector2( #{x*256}, #{y*256} )"
    portal_hash[:direction] = "Vector2( 0, -1 )"
    portal_hash[:rotation] = (Math::PI/2)
  end

  # Select the appropriate door type
  if type < 6
    portal_hash[:texture] = ($config_hash[:doors][:door_types].detect { |door| door.transform_keys(&:to_sym)[:door_id] == type}).transform_keys(&:to_sym)[:door_texture]
  else
    #Draw a blank portal for lighting and vtt purposes
    portal_hash[:texture] = "res://textures/ui/null.png"
  end

  # if drawing doors as objects then wrie it out
  if $config_hash[:doors][:door_as_objects]
    draw_object(x,y,portal_hash[:rotation],1.0,portal_hash[:texture],$config_hash[:doors][:door_layer],false)
  else
    # Write it out to the hash
    portal_hash[:node_id] = $next_node_id
    $base_hash['world']['levels']['0']['portals'] << portal_hash
    $next_node_id += 1
  end

  # If we are drawing shadows with Kragers then draw two paths either side of the door
  if $kragers == true
    path_array = Array.new
    path_array[0] = x - 0.5 * dir_y.abs()
    path_array[1] = y - 0.5 * dir_x.abs()
    path_array[2] = path_array[0] + dir_y.abs()
    path_array[3] = path_array[1] + dir_x.abs()
    draw_shadow_path(path_array, 1, 1, 128*$config_hash[:shadows][:shadow_width])
    path_array.rotate!(2)
    draw_shadow_path(path_array, 1, 1, 128*$config_hash[:shadows][:shadow_width])
  end
end

# Draw Doors onto the map
def draw_doors(map_size, data_hash)

  # 'doors' data structure is:
  # door['type'] == 0 <- texture only no door
  # door['type'] == 1 <- normal door
  # door['type'] == 2 <- door opening but no door
  # door['type'] == 3 <- staircase down
  # door['type'] == 4 <- portcullis doorway
  # door['type'] == 5 <- double door
  # door['type'] == 6 <- secret door
  # door['type'] == 8 <- staircase down

  # For each door in the watabou file
  data_hash['doors'].each_with_index do |door, idx|

    # Do nothing as Door Type 0 is some texture, Door type 2 is an opening, Door type 6 is a secret door
    case door['type']
    # Opening or just some texture
    when 0, 2
      # Do nothing as Door Type 0 is some texture, Door type 2 is an opening, Door type 6 is a secret door
    # Staircase
    when 3, 8
      # Make a staircase - find the rotation
      case "#{door['dir']['x']},#{door['dir']['y']}"
      when "0,1"
        rotation = 0
      when "0,-1"
        rotation = Math::PI
      when "-1,0"
        rotation = (Math::PI/2)
      when "1,0"
        rotation = -(Math::PI/2)
      else
        puts "Error in draw_doors"
        exit
      end
      # If 5ft squares then use the 1x1 texture
      if $scale == 1
        draw_object(door['x'] + 0.5,door['y'] + 0.5,rotation,1,$config_hash[:doors][:staircase_1x1_texture],100,false)
      elsif
        case $config_hash[:doors][:wide_staircase_type]
        when "1x2"
          x = door['x'] + 0.5 * (1 + door['dir']['x'].abs())
          y = door['y'] + 0.5 * (1 + door['dir']['y'].abs())
          draw_object(x,y,rotation,1,$config_hash[:doors][:staircase_1x2_texture],100,false)
          draw_object(x+door['dir']['y'].abs(),y+door['dir']['x'].abs(),rotation,1,$config_hash[:doors][:staircase_1x2_texture],100,false)
        when "2x1"

        when "2x2"
          x = door['x'] + 0.5 * (1 + door['dir']['x'].abs()+door['dir']['y'].abs())
          y = door['y'] + 0.5 * (1 + door['dir']['y'].abs()+door['dir']['x'].abs())
          draw_object(x,y,rotation,1,$config_hash[:doors][:staircase_2x2_texture],100,false)
        end
      end
    # Normal door or Portcullis
    when 1, 4
      if $scale == 1
        draw_normal_door(door['x']+ 0.5 * $scale,door['y']+ 0.5 * $scale,door['dir']['x'],door['dir']['y'],door['type'])
      else
        x = door['x'] + 0.5 + 0.5 * door['dir']['x'].abs()
        y = door['y'] + 0.5 + 0.5 * door['dir']['y'].abs()
        draw_normal_door(x,y,door['dir']['x'],-door['dir']['y'],door['type'])
        x += door['dir']['y'].abs()
        y += door['dir']['x'].abs()
        # Mirrored version of door type 1
        draw_normal_door(x,y,door['dir']['x'],door['dir']['y'],0)
      end
    # Double Doors
    when 5
      if $scale == 1
        draw_normal_door(door['x']+ 0.5,door['y']+ 0.5,door['dir']['x'],door['dir']['y'],door['type'])
      else
        x = door['x'] + 0.5 + 0.5 * door['dir']['x'].abs()
        y = door['y'] + 0.5 + 0.5 * door['dir']['y'].abs()

        draw_normal_door(x,y,door['dir']['x'],-door['dir']['y'],door['type'])
        x += door['dir']['y'].abs()
        y += door['dir']['x'].abs()
        # Mirrored version of door type 1
        draw_normal_door(x,y,door['dir']['x'],door['dir']['y'],0)
      end
    # Secret Door
    when 6
      # Draw wall for secret door

      if $config_hash[:doors][:draw_secret_doors]
        path_array = Array.new

        # Draw the first wall remembering that if door['dir']['y'].abs() is 1 then reverse the direction so the internal wall is always facing away from the secret door
        path_array[0] = door['x'] + 0.0 * door['dir']['x'].abs()
        path_array[1] = door['y'] + 0.0 * door['dir']['y'].abs()
        path_array[2] = path_array[0] + door['dir']['y'].abs() * $scale
        path_array[3] = path_array[1] + door['dir']['x'].abs() * $scale
        draw_wall(path_array,(door['dir']['y'].abs()==1),false)

        if $kragers == true
          draw_shadow_path(path_array, 0, 1, 256*$config_hash[:shadows][:shadow_width])
          path_array.rotate!(2)
          draw_shadow_path(path_array, 0, 1, 256*$config_hash[:shadows][:shadow_width])
        end

        # Draw the second wall remembering that doing the opposite so the internal wall is always facing away from t
        path_array[0] = door['x'] + 1.0 * door['dir']['x'].abs() * $scale
        path_array[1] = door['y'] + 1.0 * door['dir']['y'].abs() * $scale
        path_array[2] = path_array[0] + door['dir']['y'].abs() * $scale
        path_array[3] = path_array[1] + door['dir']['x'].abs() * $scale
        draw_wall(path_array,(door['dir']['y'].abs()==0),false)

        if $kragers == true
          draw_shadow_path(path_array, 0, 1, 256*$config_hash[:shadows][:shadow_width])
          path_array.rotate!(2)
          draw_shadow_path(path_array, 0, 1, 256*$config_hash[:shadows][:shadow_width])
        end
      end
    end
  end
end

# Update the core hash to have the right map size
def update_core_map_hash(map_size)

  # Read node_id if it is non-zero
  $next_node_id = $base_hash['world']['next_node_id'].to_i(16)

  # Update map size
  $base_hash['world']['width'] = map_size[2]
  $base_hash['world']['height'] = map_size[3]

  $base_hash['header']['editor_state']['camera_position'] = "Vector2( #{map_size[2]/2*256}, #{map_size[3]/2*256} )"
  $base_hash['header']['editor_state']['camera_zoom'] = 8

  # Update Terrain
  str = " 255, 0, 0, 0," * (map_size[2]*map_size[3]*16)
  $base_hash['world']['levels']['0']['terrain']['splat'] = "PoolByteArray(#{str.chop})"

  # Update Cave arrays to have 2xy + 1.5x + 1.5y + 2 (round down) separate entries. No idea why this is.
  str = " 0," * (map_size[2]*map_size[3]*2 + 1.5 * (map_size[2] + map_size[3]) + 2).floor()
  $base_hash['world']['levels']['0']['cave']['bitmap'] = "PoolByteArray(#{str.chop})"
  $base_hash['world']['levels']['0']['cave']['entrance_bitmap'] = "PoolByteArray(#{str.chop})"

end

# Draw columns if they exist
def draw_columns(map_size, data_hash)
  # Cycle through all the columns and draw them
  data_hash['columns'].each do |column|
    column['x'] = column['x'] - map_size[0] + 1
    column['y'] = column['y'] - map_size[1] + 1
    #Put a couple of shadows underneath the columns
    if $kragers == true
      for i in 1..$config_hash[:columns][:shadow_num] do
        draw_object(column['x'],column['y'],0,$config_hash[:columns][:shadow_size],$config_hash[:shadows][:shadow_corner_f_texture],400,false)
      end
    end
    # Draw in the column object
    draw_object(column['x'],column['y'],0,1,$config_hash[:columns][:column_texture],400,false)
  end
end

# Overwrite config values
def implement_config(loaded_config_hash)

  #Write the loaded values into the main config hash
  loaded_config_hash.each do |key,value|
    if value.is_a?(Hash)
      value.each do |nested_key,nested_value|
        $config_hash[key.to_sym][nested_key.to_sym] = nested_value
      end
    end
  end

  # Write in general parameters
  $scale = $config_hash[:general][:scale]
  $kragers = $config_hash[:shadows][:use_kragers]

end

# MAIN CODE
# Check that there is an input file specified and display options

parser = OptionParser.new do |opts|
	opts.banner = "Usage: convert_watabou_to_dd.rb <wataboufile.json> [options]"

  opts.on('-g config_file', 'Include wide doors for 10ft corridors') do |config_file|
    #$config_hash = JSON.parse(File.read(config_file));
    loaded_config_hash = JSON.parse(File.read(config_file));
    #puts "Reading config file: #{JSON.pretty_generate($config_hash)}";
    result = implement_config(loaded_config_hash);
  end

	opts.on('-h', '--help', 'Displays Help') do
		puts opts
		exit
	end
end
parser.parse!

# Check there is an input file specified
if ARGV.length < 1 then
  puts 'Not enough files specified.'
  puts "Usage: <watabou 1-page dungeon json filename>"
  exit
end


# Read the watabou file into a hash
data_hash = JSON.parse(File.read(ARGV[0]))

# Say Stuff
puts "\nProcessing Watabou JSON file: #{ARGV[0]}"
if $kragers == true
  puts "Creating shadows using Krager's Shadows and Lights. Note this asset pack must be in your Dungeondraft asset folder."
end


# Read the base Dungeondraft file into a hash
$base_hash = JSON.parse(File.read($config_hash[:general][:baseline_map_file]))
$next_node_id = 0

# If using 10' corridors then scale map up
if $scale > 1
  scale_map(data_hash)
end

# Find the size of the map so we can correct to axes starting at 0
map_size = find_floor_size(data_hash)

if map_size[2] > 128 || map_size[3] > 128
  puts "Map Size exceeds Dungeondraft size limits when scaled up to 10ft squares: #{map_size[2]}x#{map_size[3]}."
  puts "Choose another source map file or 5th square option."
  puts "Halting script."
  exit
end

if $kragers == true
  add_custom_asset_packs()
end

# Create an array in x,y that references the bottom left vertex
# map[x,y] format is ABCD-N where ABCD describes the squares around the vertex
# A B
#  X
# D C
# And ABCD can take values 0 = No Room or Corridor, C = Corridor square, R = Room square
# N is defined as whether a wall has been drawn on this point
$map = Array.new(map_size[2]) {Array.new(map_size[3]) {"0000-0"}}
$polygons = Array.new(data_hash['rects'].length) {Array.new {Array.new}}

result = update_core_map_hash(map_size)

result = build_map_file(map_size, data_hash)
puts "Built core data structure. Map size is #{map_size[2]}x#{map_size[3]}."

if $config_hash[:lights][:mood_lighting] == true
  result = scatter_mood_lighting()
  puts "Adding lighting effects."
end


result = draw_all_walls()
puts "Walls drawn."

#Draw flooring whether by patterns or using smart tiles
if $config_hash[:floors][:floor_as_pattern]
  result = create_pattern_floor()
  #Add default values for the smart floor references
  poolintarray = Array.new(map_size[2]*map_size[3],-1)
  $base_hash['world']['levels']['0']['tiles']['cells'] = pistr(poolintarray)
else
  result = create_smart_tile_floor(map_size)
end

# Write the terrain type into the first terrain slot
$base_hash['world']['levels']['0']['terrain']['texture_1'] = $config_hash[:general][:terrain_type]
puts "Floor drawn."

result = draw_doors(map_size, data_hash)
puts "Doors drawn."

result = draw_columns(map_size, data_hash)
puts "Columns drawn."

# Open an output file and put the new hash into it in JSON format
File.open(ARGV[0].gsub(".json",".dungeondraft_map"), "wb") { |file| file.puts JSON.pretty_generate($base_hash) }
puts "Dungeondraft file created: #{ARGV[0].gsub(".json",".dungeondraft_map")}."
puts ""
puts ""
exit
