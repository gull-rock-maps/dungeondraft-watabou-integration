# Dungeondraft Watabou Integration

Script to convert Watabou One Page Dungeon JSON files into Dungeondraft map file format

## Introduction & Purpose

The purpose of this script is to reduce the time it takes to make a simple dungeon map using Dungeondraft leveraging the AI capabilities of the Watabou One Page Dungeon generator which can then be customised as needed.

It isn't difficult or that time consuming to export the Watabou dungeon as a png from the website, import it into Dungeondraft using the trace tool and trace over it with the wall and floor tools. However it does take some time even if you are very familiar with Dungeondraft and if like me you want to add shadows and lights, it takes a little longer.

I developed this more just to see if I could as, while I did used to write simple scripts a long time ago, I am not a developer (as anyone examining my work will clearly see). There is limited error checking but it should work for the most part.

The script is written in Ruby and intended to be run from a command line. There are no fancy UI elements as that is well beyond me!

## Watabou One Page Dungeon Generator

The [Watabou One Page Dungeon generator](https://watabou.itch.io/one-page-dungeon) is an excellent resource that quickly creates dungeon maps including walls, doors, columns, water and prompts for ideas/content.  There are a number of options available for creating dungeons. The generator includes a JSON export function as well as the PNG image export and this is what we will be using to create our Dungeondraft map.

Watabou JSON file includes walls, doors, stairs, DM notes, water and columns. Some of the elements seen in the Watabou PNG, e.g. curtains, dias, objects, are not contained within the JSON export and therefore can not be brought across.

## Ruby Language

Ruby is included natively on MacOS and Linux but it needs to be downloaded I understand onto Windows devices. This can be found here: https://rubyinstaller.org/downloads/.

## Script Scope

The script supports the following features:

* Drawing of walls (supporting one-sided wall textures) from Watabou JSON
* Drawing of doors (including secret doors) from Watabou JSON
* Drawing of staircases from Watabou JSON
* Drawing of columns  from Watabou JSON
* Drawing of floor areas using the Dungeondraft building tool
* Drawing of floor areas using the Dungeondraft pattern tool
* Addition of drawing shadows (using paths and objects) under walls, doors and columns
* Scaling the map to assume Watabou dungeon uses 10' squares
* Inclusion of scattered Dungeondraft lights in resulting dungeon areas using a simple random algorithm
* All assets choices and options configured in an external config file

Out of scope

* Drawing of water areas from Watabou JSON
* Drawing of DM notes from Watabou JSON

## User Guide

### Basic Operation

1. Download the files "convert_watabou_to_ddv1.1.73.rb", "baseline.dungeondraft_map" and "config-1.json" and put them into a suitable directory
2. For shadows features, download [Kragers asset pack](https://cartographyassets.com/assets/7713/kragers-shadow-light-pack/) and put it in your Dungeondraft asset folder. 
2. Go to [Watabou One Page Dungeon generator](https://watabou.itch.io/one-page-dungeon) and download a map JSON file into the same directory
3. Run the command as below and the script will create a file "watabou-filename.dungeondraft_map" that can be opened in Dungeondraft


```
ruby convert_watabou_to_ddv1.1.73.rb <watabou-filename.json>
```

### Modifying and Using the Config File

The config file, e.g. "config-1.json", is a simple JSON file that contains the various configuration parameters for conversion. Most parameters are available as a customised option but they often require knowing the exact texture path of the asset in question which you would need to extract from an example dungeondraft_map file opened in a text editor. Unless you want to make a series of maps using the same assets, I would use DD itself to change, say, wall and pattern textures and modify the shadow and light parameters in the config file only.

The config file itself is called as a parameter when running the script so you can have multiple config files for different map themes.

```
ruby convert_watabou_to_ddv1.1.73.rb <watabou-filename.json> [-g <config-1.json>]
```

Detailed instructions for modifying the config file and parameters contained within can be found below.

### Using 3rd Party Assets

A lot of people, myself included, prefer to use 3rd party assets in Dungeondraft rather than the in built assets. The process for doing this is relatively straightforward:

* Open the "baseline.dungeondraft_map" in Dungeondraft
* Add the asset packs that you want to use to that map file and save
* Optionally, if you save it as a different file name, you need to update the config file accordingly
* Extract the texture names as needed, noting that for walls and patterns it may be quicker just to click and replace in DD once the new map file is made.


### Detailed Configuration File Guidance

The config file is a simple JSON format and you can edit it in any text editor. I would always recommend checking it with the JSON validator once done as it is easy to make typos.

A number of config file entries are direct references to asset textures. To find these references to include in the config file, the easiest way is to open any blank DD file and add any relevant 3rd party assets packs. Then add the asset that you are looking for, e.g. a door portal asset, and save the file. Open the DD map file in a text editor and search for that reference, e.g. "portals" search will take you to the JSON section containing portals and you will see an entry for the door portal that you added.

**Header Section**

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| config_name      | Text       | Yes   | A name for the config file that will be displayed when the config is used | My Favourite Config File |
| version   | Text        | No      | A version identifier - this is not used in the script and is simply for your own needs. | v1.0 |
| asset_style   | Text        | No      | A description of the 3rd party assets used - this is not used in the script and is simply for your own needs. | 2MTT |

**General Section**

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| scale      | Integer       | No   | "1" means 5ft squares, "2" means 10ft sqaures | 2 |
| baseline_map_file   | Text        | No      | The filename of the baseline map file | baseline.dungeondraft_map |
| terrain_type   | Text        | No      | The texture of the terrain used as a background for the app. | res://textures/terrain/terrain_swamp.png |

**Lights Section**

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| mood_lighting      | Boolean       | No   | If true, then reduce overall light level and scatter lights according to config  | true |
| mood_colours   | array of hex values        | No      | An array of hex values for light colours in general assuming 100 alpha | ["6480beff","644dd569","64ce4dd5"] |
| light_density   | Float        | No      | A value of density of lights | 0.25 |
| light_range   | Float        | No      | A value of range of lights | 3.0 |
| light_wall_distance   | Float        | No      | The minimum distance of a light from the walls of a room | 1.5 |
| light_intensity   | Float        | No      | The intensity of the lights | 0.6 |
| light_texture   | Float        | No      | The light texture used | res://textures/lights/soft.png |
| ambient_mood_light   | Float        | No      | The light colour used for the environment | "ff8c8c8c" |


**Walls Section**

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| wall_texture      | Text       | No   | The texture of the wall asset  | res://packs/pJHFuyES/textures/walls/dungeonbuilder_wall.png |
| wall_colour   | Text        | No      | The hex colour applied to the wall asset | ffffff |
| joint   | Integer        | No      | "0" means sharp corners, "1" means bevelled corners. | 0 |

**Floors Section**

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| floor_as_pattern      | boolean       | No   | "true" if the floors are being drawn as patterns rather than the building tool  | true |
| room_colour   | Text        | No      | The hex colour applied to the floor pattern or tile  | ffffff |
| floor_layer   | Integer        | No      | The layer used to draw the floor if being drawn as a pattern | -100 |
| floor_texture   | Text        | No      | The texture of the pattern used if being drawn as a pattern | res://packs/2EwqxwVS/textures/patterns/normal/2M Dungeon Builder Floor.webp |
| tile_texture   | Text        | No      | The texture of the tile used if being drawn as a building tool tile | res://packs/pJHFuyES/textures/tilesets/smart_double/dungeonbuilder_floor.png |

**Columns Section**

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| column_texture      | Text       | No   | The asset texture for the columns | "res://packs/pJHFuyES/textures/objects/dungeonbuilder_pillar, round (1x1).webp" |
| shadow_size   | Float        | No      | The size multiplier of the shadow object under the column which is dependent on the column asset size | 0.8 |
| shadow_num   | Integer        | No      | The number of shadow objects placed under each column | 3 |

**Doors Section**

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| skip_doorway     | Boolean       | No   | For scale 2 maps, there is an option to draw in a narrower single square doorway, if set to "true" this is not done  | true |
| door_as_objects   | Boolean        | No      | If true then doors will be drawn as bojects rather than portals | true |
| door_layer   | Integer        | No      | For doors defined as objects, this sets the layer at which the door is drawn, usually this would be 400 (under walls) or 700 (over walls) | 0 |
| staircase_1x1_texture   | Text        | No      | Defines the asset texture used for the staircase if a 1x1 asset |  |
| staircase_1x2_texture   | Text        | No      | Defines the asset texture used for the staircase if a 1x2 asset |  |
| staircase_2x2_texture   | Text        | No      | Defines the asset texture used for the staircase if a 2x2 asset |  |
| wide_staircase_type   | Text        | No      | For scale 2 maps, defines whether the staircase asset is "1x2" or "2x2" | "2x2" |
| door_types   | Array        | No      | An array of JSON objects defining the door types used as defined below | n/a |

*door_types format*

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| door_id     | Integer       | Yes   | A fixed set of values that are used by the script: "0", "1", "4" and "5"  | "5" |
| door_description   | Text        | Yes      | Don't edit this, it is here to better explain what each door_id represents when reading the config JSON | n/a |
| door_texture   | Text        | Yes      | The asset texture (either of the object or portal) that will be used for each door type |  |


**Shadows Section**

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| use_kragers      | boolean       | No   | "true" to draw in shadows using Kragers Shadow pack  | true |
| shadow_width   | float        | No      | Multiplier on the width of the shadow with "1.0" being one square  | 1.0 |
| shadow_path_texture   | Text        | No      | The texture of the shadow path used in Kragers with the key variable being darkness at the edge, e.g. 25, 50, 75, 100 | res://packs/XpMDQfBe/textures/paths/Shadow/ShadowPath/ShadowPathMed50.png |
| shadow_corner_h_texture   | Text        | No      | The texture of the half circle shadow object used in Kragers with the key variable being darkness at the edge, e.g. 25, 50, 75, 100 | res://packs/XpMDQfBe/textures/objects/ColorableGradient/Circle/Medium/ColorableGradientHalfCircleMedium50.png |
| shadow_path_texture   | Text        | No      | The texture of the quarter circle shadow object used in Kragers with the key variable being darkness at the edge, e.g. 25, 50, 75, 100 | res://packs/XpMDQfBe/textures/objects/ColorableGradient/Circle/Medium/ColorableGradientQuarterCircleMedium50.png |
| shadow_path_texture   | Text        | No      | The texture of the shadow object used under columns with the key variable being darkness at the edge, e.g. 25, 50, 75, 100 | res://packs/XpMDQfBe/textures/objects/ColorableGradient/Circle/Medium/ColorableGradientFullCircleMedium50.png |




